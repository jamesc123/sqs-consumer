package main

import (
	"bytes"
	"errors"
	"flag"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/sirupsen/logrus"
)

var (
	webhookURL = flag.String("webhook", "", "Webhook URL to POST message to")
	queueURL   = flag.String("queue", "", "Queue URL")
	verbose    = flag.Bool("verbose", false, "")
)

func init() {
	// Log as JSON instead of the default ASCII formatter.
	logrus.SetFormatter(&logrus.JSONFormatter{})

	// Output to stdout instead of the default stderr
	// Can be any io.Writer, see below for File example
	logrus.SetOutput(os.Stdout)

	flag.Parse()
}

func main() {
	// Creating a new sqs client.
	sess, err := session.NewSession(&aws.Config{
		Region: aws.String(os.Getenv("AWS_REGION")),
	})

	if err != nil {
		logrus.Fatalf(err.Error())
		return
	}

	svc := sqs.New(sess)

	for {
		msg, err := svc.ReceiveMessage(&sqs.ReceiveMessageInput{
			QueueUrl:            queueURL,
			MaxNumberOfMessages: aws.Int64(10),
		})
		if err != nil {
			logrus.Fatalf(err.Error())
			return
		}
		for _, msg := range msg.Messages {

			if *verbose {
				fmt.Println(*msg.Body)
			}

			Process(msg, svc)

		}
	}
}

// Process -
// Forward SQS message
func Process(msg *sqs.Message, svc *sqs.SQS) {
	if *webhookURL != "" {
		err := SendToWebhook(msg)
		if err != nil {
			logrus.Infof("Error is: " + err.Error())
			logrus.Error(err)

			// Release message straight back to the queue to be retried
			svc.ChangeMessageVisibility(&sqs.ChangeMessageVisibilityInput{
				QueueUrl:          queueURL,
				ReceiptHandle:     msg.ReceiptHandle,
				VisibilityTimeout: aws.Int64(0),
			})

			return
		}
	}

	// Remove message from the queue
	_, err := svc.DeleteMessage(&sqs.DeleteMessageInput{
		QueueUrl:      queueURL,
		ReceiptHandle: msg.ReceiptHandle,
	})
	if err != nil {
		logrus.Fatalf(err.Error())
	}
}

// SendToWebhook -
// Send SQS message to a webhook
func SendToWebhook(msg *sqs.Message) error {
	var jsonStr = []byte(*msg.Body)
	req, err := http.NewRequest("POST", *webhookURL, bytes.NewBuffer(jsonStr))
	req.Header.Set("X-Custom-Header", "myvalue")
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	client.Timeout = time.Second * 10
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	body, _ := ioutil.ReadAll(resp.Body)

	if resp.StatusCode >= 200 && resp.StatusCode < 400 {
		return nil
	}

	return errors.New(string(body))
}
